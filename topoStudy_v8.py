import dxfgrabber
import numpy as np
import pandas as pd
import math
from shapely.geometry import LineString, Point
from scipy.interpolate import griddata, interp1d
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.lines import Line2D  
from shapely import affinity



# :: Script to process contour + layout drawing ::


# : Classes :
#TODO: Add some up in here!


# : Inputs :

# Selected truss number
folderPath = r"C:\Users\Contractor03\Ojjo\Ojjo Company Site - Product\Analysis\Bidding\Aragorn\Topo Analysis"
forgettenFile = "\ForgettenRows.csv"
forgottenPath = folderPath + forgettenFile
forgottenRows = pd.read_csv(forgottenPath)

j=0 #<--Truss id to print
plotGraph = True
doAll = False
trackerLabels = ['5EE3C']
# trackerLabels =forgottenRows.iloc[:,0].values
outFileName = 'testt.csv'

# Static Geometery
Avalue =75/12 # feet
Bvalue =90/12 # feet
Rvalue = 13/12 # feet

# Layout properties
moduleLength = 2009/25.4/12 # feet
moduleWidth = 1232/25.4/12 # feet
moduleSpacing = 0.5/12 # feet
trackerGCRTopoDist = 200 # feet
plotOutline = trackerGCRTopoDist/2
hPiletohWp = 9.43/12 # feet
minPileReveal = 39/12 # feet
min_hwp = hPiletohWp + minPileReveal # feet
# maxPileReveal = 78/12 # feet
# max_hwp = hPiletohWp + maxPileReveal # feet
torqueTubeThickness = 6/12 # feet
maxTrussProfile = 18*12/12 # feet


# : Load in Data :

# Load the contour points 
print('Reading Contour Data')
contourFile = r"\AragornPointcloud_v2.csv"
contourPath = folderPath + contourFile
contourSuracePoints = pd.read_csv(contourPath)

# Load in pile location data
print('Reading Pile Data')
rowLengthDict = {
    "A":330.8,
    "B":313.99,
    "C":289.59,
    "D":264.64
}
pileLocationFile = r"\PileLocations_v1.csv"
pileLocationPath = folderPath + pileLocationFile
pileLocations_df = pd.read_csv(pileLocationPath)

# read in the tracker locations
print('Reading Tracker Outlines')
trackerTables = pd.read_csv('trackerOutline.csv')

# : Save Lists :
hPileReveal = []
Cdist = []
bSlope = []
ttSlope = []
trackerId = []

fig = plt.figure()
gs = fig.add_gridspec(8, 8)

# : Methods :

def getTrussPoints(distanceArray, torque_tube):
    """Returns a list of points along a torque tube at required (x) distances"""
    retList = []
    xRet = []
    yRet = []
    for distance in distanceArray:
        newPt = torque_tube.interpolate(distance)
        retList.append(newPt)
        xRet.append(newPt.x)
        yRet.append(newPt.y)
    return retList, xRet, yRet

def buildInstallGeometry(truss_df, torque_tube, surfaceTopoLinestring):
    """Develops install geometry of Truss Driver"""
    # create some new columsn
    Alines = []
    Blines = []
    Clines = []
    HLines = []

    # scale up the torque tube line to allow for the process, 4x should be enough
    scaledTT = affinity.scale(torque_tube,4,4)
    # ~the scaled TT is now the source of truth~
    # start point of TT, and other nice things
    originalStartPoint = Point(torque_tube.coords[0])
    baseOffsetDistance = Point(scaledTT.coords[0]).distance(originalStartPoint)
    direction = np.sign(torque_tube.coords[0][0]-torque_tube.coords[1][0])

    # Loop through each truss point
    for trussRow in truss_df.iterrows():
        # *this should be direction agnostic*
        truss = trussRow[1][0]
        trussOffsetDistance = truss.distance(Point(torque_tube.coords[0]))
        trussDistance = trussOffsetDistance + baseOffsetDistance
        # Point A
        pointA = scaledTT.interpolate(trussDistance-Avalue)
        # trussPoint = scaledTT.interpolate(trussDistance)
        # grade projection
        ALine_inf = affinity.rotate(LineString([(pointA.x,pointA.y), (Point(scaledTT.coords[0]).x, Point(scaledTT.coords[0]).y)]), -90, origin = pointA)
        gradeA = ALine_inf.intersection(surfaceTopoLinestring)
        # Make sure that this doesnt blow up in the future!
        Aline = LineString([(gradeA.x, gradeA.y), (pointA.x, pointA.y)])
        Alines.append(Aline)
        BsubApt = Aline.interpolate(Rvalue)
        AgradeFlatLine = LineString([(gradeA.x, gradeA.y), (gradeA.x+(direction*100), gradeA.y)])

        # determine slope of local area around front tread wheel
        hs = []
        for i in range(-1,1,2):
            d = Rvalue+i
            testPt = AgradeFlatLine.interpolate(Bvalue+d)
            testLine = LineString([(testPt.x, testPt.y-100), (testPt.x, testPt.y+100)])
            testPtHeight = testLine.intersection(surfaceTopoLinestring).y-testPt.y
            hs = np.append(hs,testPtHeight)
        averageYChange = np.average(hs)

        # B Line
        gradeSlope = math.degrees(math.atan((averageYChange)/Bvalue))
        Bline = affinity.rotate(LineString([(BsubApt.x,BsubApt.y), (BsubApt.x+(direction*Bvalue), BsubApt.y)]),gradeSlope,BsubApt)
        Blines.append(Bline)

        # C Line
        # do some work to prepare for the C line calcualtion
        track_GradeLine = affinity.rotate(LineString([(gradeA.x, gradeA.y), (gradeA.x-(direction*100), gradeA.y)]),gradeSlope,gradeA)
        CinfLine = affinity.rotate(LineString([truss,(truss.x, truss.y-100)]),gradeSlope, truss)
        CBasePoint = CinfLine.intersection(track_GradeLine)
        Clines.append(LineString([CBasePoint,truss]))

        # H Line
        hInfLine = LineString([truss,(truss.x, truss.y-100)])
        hPoint = hInfLine.intersection(surfaceTopoLinestring)
        HLines.append(LineString([truss,hPoint]))
    
    #reverse all of the lists
    # Alines.reverse()
    # Blines.reverse()
    # Clines.reverse()
    # HLines.reverse()
    return Alines, Blines, Clines, HLines

def designTrackerRow(tracker, extraReveal):
    # develop north & south-most module edge
    northEdge = LineString([(tracker[1]['minx'], tracker[1]['maxy']),(tracker[1]['maxx'], tracker[1]['maxy'])])
    southEdge = LineString([(tracker[1]['minx'], tracker[1]['miny']),(tracker[1]['maxx'], tracker[1]['miny'])])
    # create torque tube in map view
    northEdgeCenter = northEdge.centroid
    southEdgeCenter = southEdge.centroid
    # northhanded!
    torqueTubeLine = LineString([(northEdgeCenter.x, northEdgeCenter.y),(southEdgeCenter.x, southEdgeCenter.y)])

    # figure out what tracker length we are dealing with
    deltaMin = 1000000000
    trackerType = 'X'
    for trackerId, trackerTypeLength in rowLengthDict.items():
        tempDelta = abs(torqueTubeLine.length - trackerTypeLength)
        if tempDelta < deltaMin:
            trackerType = trackerId
            deltaMin = tempDelta

    # load in the surface data that is within a determined box around this tracker table
    eastX = northEdgeCenter.x + trackerGCRTopoDist
    westX = northEdgeCenter.x - trackerGCRTopoDist
    northY = northEdgeCenter.y + trackerGCRTopoDist
    southY = southEdgeCenter.y - trackerGCRTopoDist
    contour_df_filtered = contourSuracePoints[(contourSuracePoints.x <= eastX) & 
                            (contourSuracePoints.x >= westX) & 
                            (contourSuracePoints.y <= northY) & 
                            (contourSuracePoints.y >= southY)]


    # : Create Topo Surface :

    # lets create a figure to show things
    ax_contour = fig.add_subplot(gs[:, :-6])
    ax_elevation = fig.add_subplot(gs[:-6, 2:])
    ax_elevation.set_ylabel('Elevtion (ft)')
    fig.subplots_adjust(hspace=2)
    fig.subplots_adjust(wspace=1.5)
    # create surface
    x = np.linspace(westX, eastX, 100)
    y = np.linspace(southY, northY, 300)
    X, Y = np.meshgrid(x,y)
    surfacexy = contour_df_filtered[['x', 'y']].values
    interpolatedSurface = griddata(surfacexy, contour_df_filtered.z.values, (X, Y), method='cubic')
    ax_contour.contourf(X, Y, interpolatedSurface)
    ax_contour.set_xlim(northEdgeCenter.x-plotOutline,northEdgeCenter.x+plotOutline)
    # add torque tube outline
    rect = patches.Rectangle((northEdgeCenter.x-moduleWidth/2, southEdgeCenter.y),moduleWidth, torqueTubeLine.length, linewidth=1,edgecolor='b',facecolor='none')
    ax_contour.add_patch(rect)
    # create flat torque tube and look at general slope of line
    pileLocationsRow = pileLocations_df[trackerType].where(lambda x: x>0).dropna()
    # northhanded! (we start from the first point we defined as on the north end earlier)
    trussLocations, trussX, trussY = getTrussPoints(pileLocationsRow, torqueTubeLine)
    ax_contour.scatter(trussX, trussY, marker = '+', c = '#faf363')
    ax_contour.set_title('Surface Contour Plot')


    # : Create Elevation View + Data :

    # pull out the points below the torque tube to map surface
    # first, locate what index we should be looking at
    xIndex = np.where(X[0]==min(X[0], key=lambda x:abs(x-northEdgeCenter.x)))[0][0]
    # pull out the surface z, x, y values
    sufurfaceValues = interpolatedSurface[:,xIndex]
    xValues = X[:,xIndex]
    yValues = Y[:,xIndex]
    # create dataframe 
    df_alongTT = pd.DataFrame({
        'x':xValues,
        'y':yValues,
        'z':sufurfaceValues
    }).dropna()
    # Create topo line underneath torque tube and plot
    f_surface = interp1d(df_alongTT.y, df_alongTT.z, kind = 'cubic')
    longYs = (np.linspace(df_alongTT.y.min(), df_alongTT.y.max(), num=500, endpoint=True))
    longZs = f_surface(longYs)
    # lets plot some sheit
    ax_elevation.plot(longYs, f_surface(longYs))
    ax_elevation.set_title('Elevation View')
    ax_elevation.set_ylim([df_alongTT.z.min()-1,df_alongTT.z.max()+10])
    # finally, create a new linestring that describes the surface topo
    surfaceTopoLinestring = LineString(list(zip(longYs,longZs)))

    # : Design Torque Tube :

    # design sloped TT to start
    # northhanded!
    numberOfTrusses = len(pileLocationsRow)
    first_z = f_surface([trussLocations[0].y])
    last_z = f_surface([trussLocations[numberOfTrusses-1].y])
    slope = math.degrees(math.atan((last_z - first_z)/(trussLocations[0].distance(trussLocations[numberOfTrusses-1]))))
    elevationTT_Line = LineString([(trussLocations[0].y, first_z),(trussLocations[numberOfTrusses-1].y, last_z)])
    # create new set of truss locations
    elevationTrussLocations, elevTrussY, elevTrussZ = getTrussPoints(pileLocationsRow, elevationTT_Line)
    # create new more granular dataframe for elevation below TT
    df_TTgradeEllevation = pd.DataFrame({'y':longYs,'z':longZs})
    # lists to save to TODO: Move these to outside the loop!
    trussVertCntrLineList = []
    vertDistanceToGrade = []
    indexJ = 0
    # loop through truss locations
    for truss in elevationTrussLocations:
        # filter out location that most closely matches the desired location
        filteredDF = df_TTgradeEllevation[df_TTgradeEllevation.y==min(longYs, key=lambda y:abs(y-truss.x))]
        # create a truss centerline 
        vertTrussCntrLine = LineString([(truss.x, truss.y+maxTrussProfile),(truss.x, truss.y-maxTrussProfile)])
        # rotate this truss to be perpendicular to the TT
        rot_trussCntrLine = affinity.rotate(vertTrussCntrLine, slope, origin=truss)
        # save this
        trussVertCntrLineList.append(rot_trussCntrLine)
        # draw line
        line = Line2D([rot_trussCntrLine.xy[0][0], rot_trussCntrLine.xy[0][1]], [rot_trussCntrLine.xy[1][0], rot_trussCntrLine.xy[1][1]], linewidth=0.5, color='black')
        ax_elevation.add_line(line)
        # Look for intersection of truss centerline and the surface
        # NOTE! we are using the vertical line,not the rotated line. This will be useful
        intersectionPoint = vertTrussCntrLine.intersection(surfaceTopoLinestring)
        # plot the truss index
        ax_elevation.text(intersectionPoint.x+5, intersectionPoint.y, indexJ, fontsize=8)
        # get vertical distance to grade and save it
        vertDistanceToGrade.append(intersectionPoint.distance(truss) * np.sign(truss.y - intersectionPoint.y))
        # add to indexer
        indexJ += 1

    # alright, so now we have our vertical distances to grade (h pile reveal equivalents) - lets adjust the TT to the correct height
    vertDistanceToGrade.reverse()
    minDistance = min(vertDistanceToGrade)
    maxDistance = max(vertDistanceToGrade)
    # this is tricky
    minRevealAdder = min_hwp + abs(minDistance)+extraReveal
    # maxRevealAdder = maxPileReveal - abs(maxDistance)
    # Minimum workpoint style
    minimumTT = LineString([(trussLocations[0].y, first_z+minRevealAdder),(trussLocations[numberOfTrusses-1].y, last_z+minRevealAdder)])
    # plot our TT
    elevTT = Line2D([minimumTT.xy[0][0], minimumTT.xy[0][1]], [minimumTT.xy[1][0], minimumTT.xy[1][1]], linewidth=4, color='grey')
    # add to plot
    ax_elevation.add_line(elevTT)
    ax_elevation.invert_xaxis()


    # : Design Workpoints : 

    # Now add new truss points to the elevated truss
    elevationTrussLocations, elevTrussY, elevTrussZ = getTrussPoints(pileLocationsRow, minimumTT)
    # Create dataframe to run analysis with (mainly to simplify nomenclature and put in Ojjo Terms)
    tt_df = pd.DataFrame({'truss_point':elevationTrussLocations,
                            'trussX':elevTrussY,
                            'trussZ':elevTrussZ})
    # Apply the Truss building functions
    A,B,C,H = buildInstallGeometry(tt_df, minimumTT, surfaceTopoLinestring)

    return A,B,C,H,minimumTT,df_TTgradeEllevation,f_surface

def getMinHpileReveal(hPileLineList):
    """Iterates through list to figure out what the minimum h pile reveal is"""
    minRevealinList = 10000000
    for hPileLine in hPileLineList:
        if hPileLine.length < minRevealinList:
            minRevealinList = hPileLine.length
    return minRevealinList


# : Process Project :

trackers = trackerTables
if not doAll:
    trackers = trackerTables[trackerTables.handle.isin(trackerLabels)]

for tracker in trackers.iterrows():
    try:
        print("working on tracker "+tracker[1]['handle'])
        Aout = []
        Bout = []
        Cout = []
        Hout = []
        torqueTubeOut = LineString()
        dtTTElev = pd.DataFrame()

        # design the row
        Aout,Bout,Cout,Hout,torqueTubeOut,dtTTElev,fSurf = designTrackerRow(tracker,0)
        # check to see if the min reveal is ok
        minHPileReveal = getMinHpileReveal(Hout)
        # little sanity check here
        if minHPileReveal<100:
            if minHPileReveal < minPileReveal:
                # we need to add a little extra here
                print("adding extra reveal")
                Aout,Bout,Cout,Hout,torqueTubeOut,dtTTElev,fSurf = designTrackerRow(tracker,(minPileReveal-minHPileReveal))
        
        # check to see if this passes
        for iTruss in range(0,len(Hout)):
            # calcualte h pile reveal
            hPileReveal = np.append(hPileReveal, Hout[iTruss].length)
            # calcualte c length
            Cdist = np.append(Cdist, Cout[iTruss].length)
            # figure out base slope
            bi = Bout[iTruss]
            xB0 = bi.coords[0][0]
            xB1 = bi.coords[1][0]
            bSlopeSave = 0
            if xB0 > xB1:
                bSlopeSave = math.degrees(math.atan((bi.coords[1][1]-bi.coords[0][1])/(xB1-xB0)))
            else:
                bSlopeSave = math.degrees(math.atan((bi.coords[0][1]-bi.coords[1][1])/(xB0-xB1)))
            bSlope = np.append(bSlope, bSlopeSave)
            #figure out tt slope
            xtt0 = torqueTubeOut.coords[0][0]
            xtt1 = torqueTubeOut.coords[1][0]
            bSlopeSave = 0
            if xtt0 > xtt1:
                ttSlopeSave = math.degrees(math.atan((torqueTubeOut.coords[1][1]-torqueTubeOut.coords[0][1])/(xtt1-xtt0)))
            else:
                ttSlopeSave = math.degrees(math.atan((torqueTubeOut.coords[0][1]-torqueTubeOut.coords[1][1])/(xtt0-xtt1)))
            ttSlope = np.append(ttSlope, ttSlopeSave)
            trackerId = np.append(trackerId, tracker[1]['handle'])
        
        # Save all of the data
        dfOut = pd.DataFrame({'hPileReveal':hPileReveal,
                                'Cdist':Cdist,
                                'bSlope':bSlope,
                                'ttSlope':ttSlope,
                                'trackerId':trackerId}).to_csv(outFileName)

        if plotGraph:
            # : Draw One Truss Configuration :

            # set up a new plot
            ax_geometry = fig.add_subplot(gs[2:6, 2:])
            ax_geometry.invert_xaxis()
            # pull out all of the values
            a = Aout[j]
            b = Bout[j]
            c = Cout[j]
            h = Hout[j]
            # filter out the terrain in the range and plot it
            elevation_filtered = dtTTElev[(dtTTElev.y>=(h.coords[1][0]-(Bvalue+20)))&(dtTTElev.y<=(h.coords[1][0]+(Bvalue+20)))]
            ax_geometry.plot(elevation_filtered.y, fSurf(elevation_filtered.y))
            # Draw B
            slopeOfTracks = math.degrees(math.atan((b.coords[0][1]-b.coords[1][1])/(b.coords[0][0]-b.coords[1][0])))
            BTrackWidth = Line2D([b.coords[0][0], b.coords[1][0]], [b.coords[0][1], b.coords[1][1]], linewidth=4, color='r')
            ax_geometry.add_line(BTrackWidth)
            # plot the truss driver base
            wheel1 = plt.Circle(b.coords[0], radius=Rvalue, fc='k')
            ax_geometry.add_patch(wheel1)
            wheel2 = plt.Circle(b.coords[1], radius=Rvalue, fc='k')
            ax_geometry.add_patch(wheel2)
            # add the torque tube
            elevTT2 = Line2D([torqueTubeOut.xy[0][0], torqueTubeOut.xy[0][1]], [torqueTubeOut.xy[1][0], torqueTubeOut.xy[1][1]], linewidth=4, color='grey')
            ax_geometry.add_line(elevTT2)
            # set y axis limit
            ax_geometry.set_ylim([elevation_filtered.z.min()-1,elevation_filtered.z.max()+10])

            # draw all of the geometry
            AplotLine = Line2D([a.coords[0][0], a.coords[1][0]], [a.coords[0][1], a.coords[1][1]], linewidth=2, linestyle = '-', color = 'aqua')
            ax_geometry.add_line(AplotLine)
            HPile = Line2D([h.coords[0][0], h.coords[1][0]], [h.coords[0][1], h.coords[1][1]], linewidth=2, linestyle = '-', color = 'orangered')
            ax_geometry.add_line(HPile)
            CLinePlot = Line2D([c.coords[0][0], c.coords[1][0]], [c.coords[0][1], c.coords[1][1]], linewidth=2, linestyle = '-', color = 'darkblue')
            ax_geometry.add_line(CLinePlot)
            ax_geometry.set_title('Truss:'+str(j)+' C='+str(round(c.length*12,2))+' Inches')
            plt.show()

    except:
        print("Tracker "+tracker[1]['handle']+" messed up")