import dxfgrabber
import numpy as np
import pandas as pd


folderPath = r"C:\Users\Contractor03\Ojjo\Ojjo Company Site - Product\Analysis\Bidding\Aragorn\Topo Analysis"
fileName = "\Aragorn_Row_Topo_v1.dxf"
fileLocation = folderPath + fileName
print('Loading Drawing')
dxf = dxfgrabber.readfile(fileLocation)
print('Finished Loading Drawing')
lines = [entity for entity in dxf.entities if (entity.dxftype == 'LWPOLYLINE') & ( entity.layer != 'SE_ELEC_MODULES')]

# loop through and save all of the points in 3d
x = []
y = []
z = []
for line in lines:
    print('working line:'+ line.handle)
    for point in line.points:
        x = np.append(x, point[0]) # inches
        y = np.append(y, point[1]) # inches?
        z = np.append(z, line.elevation) # in feet

# Create a new dataframe with this inforamtion
pointcloudDF = pd.DataFrame({'x':x,
                             'y':y,
                             'z':z})

pointcloudDF.to_csv('aragornPointcloud_v2.csv')
h=8