import dxfgrabber
import numpy as np
import pandas as pd



if True:
    # Load drawing and objects
    fileName = r"\Aragorn_Row_Topo_v1.dxf"
    fileLocation = folderPath + fileName
    print('Loading Drawing')
    dxf = dxfgrabber.readfile(fileLocation)

    # pull out all of the tracker outlines from the drawing
    trackerTables = [entity for entity in dxf.entities if entity.layer == 'SE_ELEC_MODULES' and entity.dxftype=='LWPOLYLINE']
    minXList = []
    maxXList = []
    minYList = []
    maxYList = []
    for iTracker in trackerTables:
        ys = [y[1] for y in iTracker.points]
        xs = [x[0] for x in iTracker.points]
        minXList.append(min(xs))
        maxXList.append(max(xs))
        minYList.append(min(ys))
        maxYList.append(max(ys))
    trackerOutlineTable = pd.DataFrame({'minx':minXList,
                                        'maxx':maxXList,
                                        'miny':minYList,
                                        'maxy':maxYList,
                                        'handle':[entity.handle for entity in trackerTables]}).to_csv('trackerOutline.csv')